package com.git.recommend.controller;

import com.git.recommend.entity.*;
import com.git.recommend.service.DataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/data")
public class DataController {

    @Autowired
    private DataService dataService;

    @RequestMapping("/getCategoryCount")
    @ResponseBody
    public List<CategoryData> getCategoryData(HttpServletRequest request){
        List<CategoryData> list = dataService.getCategoryData();
        return list;
    }

    @RequestMapping("/getRecommendGoodsByWaitTime")
    @ResponseBody
    public List<RecommendGoods> getRecommendGoodsByWaitTime(HttpServletRequest request){
        List<RecommendGoods> list = new ArrayList<>();
        list = dataService.getRecommendGoodsByWaitTime(Integer.parseInt(request.getParameter("user_id")));
        return list;
    }

    @RequestMapping("/getRecommendGoodsByProvince")
    @ResponseBody
    public List<RecommendGoods> getRecommendGoodsByProvince(HttpServletRequest request){
        List<RecommendGoods> list = new ArrayList<>();
        list = dataService.getRecommendGoodsByProvince(Integer.parseInt(request.getParameter("user_id")));
        return list;
    }

    @RequestMapping("/getGoodsTopData")
    @ResponseBody
    public List<GoodsTopData> getGoodsTopData(HttpServletRequest request){
        List<GoodsTopData> list = dataService.getGoodsTopData();
        return list;
    }
    @RequestMapping("/getCommentsData")
    @ResponseBody
    public List<CommentsData> getCommentsData(HttpServletRequest request){
        List<CommentsData> list = dataService.getCommentsData();
        return list;
    }

    @RequestMapping("/getProvinceAvgTIme")
    @ResponseBody
    public List<ProvinceAvgTime> getProvinceAvgTime(HttpServletRequest request){
        List<ProvinceAvgTime> list = dataService.getProvinceAvgTime();
        return list;
    }
    @RequestMapping("/getUserInfo")
    @ResponseBody
    public List<UserInfo> getUserInfo(HttpServletRequest request){
        List<UserInfo> list = dataService.getUserInfo(Integer.parseInt(request.getParameter("user_id")));
        return list;
    }

    @RequestMapping("/getAllUserId")
    @ResponseBody
    public List<Integer> getUserId(HttpServletRequest request){
        List<Integer> list = new ArrayList<>();
        list = dataService.getUserId();
        return list;
    }
    @RequestMapping("/getUserAction")
    @ResponseBody
    public List<UserActionCount> getUserAction(HttpServletRequest request){
        List<UserActionCount> list = new ArrayList<>();
        list = dataService.getUserActionCount(Integer.parseInt(request.getParameter("user_id")));
        return list;
    }

    @RequestMapping("/getCategoryTime")
    @ResponseBody
    public List<CategoryTime> getCategoryTime(HttpServletRequest request){
        List<CategoryTime> list = new ArrayList<>();
        list = dataService.getCategoryTime(Integer.parseInt(request.getParameter("user_id")));
        return list;
    }

    @RequestMapping("/getBuyCar")
    @ResponseBody
    public List<String> getBuyCar(HttpServletRequest request){
        List<String> list = new ArrayList<>();
        list = dataService.getBuyCar(Integer.parseInt(request.getParameter("user_id")));
        return list;
    }

    @RequestMapping("/getFavorite")
    @ResponseBody
    public List<String> getFavorite(HttpServletRequest request){
        List<String> list = new ArrayList<>();
        list = dataService.getFavorite(Integer.parseInt(request.getParameter("user_id")));
        return list;
    }
    @RequestMapping("/getRefundInfo")
    @ResponseBody
    public List<RefundInfo> getRefund(HttpServletRequest request){
        List<RefundInfo> list = new ArrayList<>();
        list = dataService.getRefund(Integer.parseInt(request.getParameter("user_id")));
        return list;
    }

}
