package com.git.recommend.entity;

public class UserActionCount {


    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(int goodsId) {
        this.goodsId = goodsId;
    }

    public int getTypeOperate() {
        return typeOperate;
    }

    public void setTypeOperate(int typeOperate) {
        this.typeOperate = typeOperate;
    }

    public long getTimes() {
        return times;
    }

    public void setTimes(long times) {
        this.times = times;
    }
    int userId;
    int goodsId;
    int typeOperate;
    long times;
}
