package com.git.recommend.entity;


//各省份平均送达时间
public class ProvinceAvgTime {
    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public double getAvgTime() {
        return avgTime;
    }

    public void setAvgTime(double avgTime) {
        this.avgTime = avgTime;
    }

    String province;
    double avgTime;
}
