package com.git.recommend.entity;

public class CommentsData {
    long allComments;

    public long getAllComments() {
        return allComments;
    }

    public void setAllComments(long allComments) {
        this.allComments = allComments;
    }

    public long getGoodComments() {
        return goodComments;
    }

    public void setGoodComments(long goodComments) {
        this.goodComments = goodComments;
    }

    public long getBadComments() {
        return badComments;
    }

    public void setBadComments(long badComments) {
        this.badComments = badComments;
    }

    long goodComments;
    long badComments;
}
