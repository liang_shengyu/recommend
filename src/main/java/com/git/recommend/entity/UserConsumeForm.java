package com.git.recommend.entity;

public class UserConsumeForm {
    int userId;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getTypeOperate() {
        return typeOperate;
    }

    public void setTypeOperate(int typeOperate) {
        this.typeOperate = typeOperate;
    }

    public int getTypeOperateCount() {
        return typeOperateCount;
    }

    public void setTypeOperateCount(int typeOperateCount) {
        this.typeOperateCount = typeOperateCount;
    }

    int typeOperate;
    int typeOperateCount;
}
