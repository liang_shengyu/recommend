package com.git.recommend.task;

import com.git.recommend.entity.*;
import com.git.recommend.service.HiveService;
import com.git.recommend.service.InsertMysqlService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Map;

@Component
public class HiveTask {
    @Autowired
    private HiveService hiveService;

    @Autowired
    private InsertMysqlService insertMysqlService;

    @PostConstruct
    public void translateData(){
        System.out.println("传输开始");
//      List<Map<String,Object>> list = hiveService.getGoodsTopN();
//      GoodsTopN goodsTopN = new GoodsTopN();
//      for (Map<String,Object> item : list){
//          goodsTopN.setGoodsId((Integer) item.get("goods_Id"));
//          goodsTopN.setCategory((String) item.get("category"));
//          goodsTopN.setGoodsName((String) item.get("goods_name"));
//          goodsTopN.setSalesCount((Long) item.get("sales_count"));
//          goodsTopN.setPublisher((String) item.get("publisher"));
//          goodsTopN.setPrice((Double) item.get("price"));
//          goodsTopN.setSellPrice((Double) item.get("sell_price"));
//          insertMysqlService.insertGoodsTopN(goodsTopN);
//        }
////        List<Map<String,Object>> list = hiveService.getAvgArriveTime();
////        AvgArriveTime avgArriveTime = new AvgArriveTime();
////        for (Map<String,Object> item : list){
////          avgArriveTime.setOrderId((String) item.get("order_id"));
////          avgArriveTime.setOrderTime((Long) item.get("order_time"));
////          avgArriveTime.setReceiveTime((Long) item.get("receive_time"));
////          avgArriveTime.setProvince((String) item.get("province"));
////          insertMysqlService.inserAvgArriveTime(avgArriveTime);
////        }
////      List<Map<String,Object>> list = hiveService.getCommentsData();
////        CommentsData commentsData = new CommentsData();
////        for (Map<String,Object> item : list){
////         commentsData.setAllComments((Long) item.get("all_comments"));
////         commentsData.setGoodComments((Long) item.get("good_comments"));
////         commentsData.setBadComments((Long) item.get("bad_comments"));
////          insertMysqlService.insertCommentsData(commentsData);
////       }
//
//        List<Map<String,Object>> list = hiveService.getProvinceInfo();
//        ProvinceInfo provinceInfo = new ProvinceInfo();
//        for (Map<String,Object> item : list){
//            provinceInfo.setProvince((String) item.get("province"));
//            provinceInfo.setCategory((String) item.get("category"));
//            provinceInfo.setSalesCount((Long) item.get("sales_count"));
//            insertMysqlService.insertProvinceInfo(provinceInfo);
//        }

//        List<Map<String,Object>> list = hiveService.getUserActionTime();
//        UserActionTime userActionTime = new UserActionTime();
//        for (Map<String,Object> item : list){
//            userActionTime.setGoodsId((Integer) item.get("goods_id"));
//            userActionTime.setUserId((Integer) item.get("user_id"));
//            userActionTime.setWaitTime((Long) item.get("wait_time"));
//            insertMysqlService.insertUserActionTime(userActionTime);
//        }

//        List<Map<String,Object>> list = hiveService.getUserActionCount();
//        UserActionCount userActionCount = new UserActionCount();
//        for (Map<String,Object> item : list){
//            userActionCount.setUserId((Integer) item.get("user_id"));
//            userActionCount.setGoodsId((Integer) item.get("goods_id"));
//            userActionCount.setTypeOperate((Integer) item.get("tpye_operate"));
//            userActionCount.setTimes((Long) item.get("times"));
//
//            insertMysqlService.insertUserActionCount(userActionCount);
//        }
//
//        List<Map<String,Object>> list = hiveService.getUserRefundInfo();
//        UserRefundInfo userRefundInfo = new UserRefundInfo();
//        for (Map<String,Object> item : list){
//            userRefundInfo.setUserId((Integer) item.get("user_id"));
//            userRefundInfo.setReasonRefund((String) item.get("reason_frefund"));
//            insertMysqlService.insertUserRefundInfo(userRefundInfo);
//        }

        System.out.println("数据传输结束");
    }
}
