package com.git.recommend.service;

import java.util.List;
import java.util.Map;

public interface HiveService {
    List<Map<String,Object>> getGoodsTopN();
    List<Map<String,Object>> getAvgArriveTime();
    List<Map<String,Object>> getCategoryHot();
    List<Map<String,Object>> getCommentsData();
    List<Map<String,Object>> getProvinceInfo();
    List<Map<String,Object>> getUserActionTime();
    List<Map<String,Object>> getUserActionCount();
    List<Map<String,Object>> getUserInfo();
    List<Map<String,Object>> getUserRefundInfo();
}
