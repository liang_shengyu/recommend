package com.git.recommend.service;

import com.git.recommend.entity.*;
import com.git.recommend.mapper.DataMapper;
import org.checkerframework.checker.units.qual.C;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class DataService {

    @Autowired
    private DataMapper dataMapper;

    public List<CategoryData> getCategoryData(){
        return dataMapper.getCategoryData();
    }

    public List<GoodsTopData> getGoodsTopData(){
        return dataMapper.getGoodsTopData();
    }

    public List<CommentsData> getCommentsData(){
        return  dataMapper.getCommentsData();
    }

    public List<ProvinceAvgTime> getProvinceAvgTime(){
        return  dataMapper.getProvinceAvgTime();
    }

    public List<RecommendGoods> getRecommendGoodsByWaitTime(int userId){
        return dataMapper.getRecommendGoodsByWaitTime(userId);
    }

    public List<RecommendGoods> getRecommendGoodsByProvince(int userId){
        return dataMapper.getRecommendGoodsByProvince(userId);
    }

    public List<UserInfo> getUserInfo(int userId) {
        return dataMapper.getUserInfo(userId);
    }

//    public List<UserConsumeForm> getUserConsumeForm(int userId) {
//        return dataMapper.getUserConsumeForm(userId);
//    }


    public List<Integer> getUserId(){
        return dataMapper.getAllUserId();
    }

    public List<UserActionCount> getUserActionCount(int userId){
        return dataMapper.getUserAction(userId);
    }

    public List<CategoryTime>getCategoryTime(int userId){
        return dataMapper.getCategoryTime(userId);
    }

    public List<RefundInfo> getRefund(int userId){
        return dataMapper.getRefund(userId);
    }

    public List<String> getBuyCar(int userId){
        return dataMapper.getBuyCar(userId);
    }

    public List<String> getFavorite(int userId){
        return dataMapper.getFavorite(userId);
    }
}
