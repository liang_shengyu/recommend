package com.git.recommend.service;

import com.git.recommend.entity.*;

public interface InsertMysqlService {
    int insertGoodsTopN(GoodsTopN goodsTopN);
    int insertAvgArriveTime(AvgArriveTime avgArriveTime);
    int insertCategoryHot(CategoryHot categoryHot);
    int insertCommentsData(CommentsData commentsData);
    int insertProvinceInfo(ProvinceInfo provinceInfo);
    int insertUserActionTime(UserActionTime userActionTime);
    int insertUserActionCount(UserActionCount userActionCount);
    int insertUserInfo(UserInfo userInfo);
    int insertUserRefundInfo(UserRefundInfo userRefundInfo);
}