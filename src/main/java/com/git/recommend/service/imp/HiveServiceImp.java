package com.git.recommend.service.imp;

import com.git.recommend.mapper.InsertMysqlMapper;
import com.git.recommend.service.HiveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class HiveServiceImp implements HiveService {

    @Autowired
    @Qualifier("hiveJdbcTemplate")
    private JdbcTemplate jdbcTemplate;


    @Override
    public List<Map<String, Object>> getGoodsTopN() {
        String sql = "select tb_goods.goods_id as goods_id,t2.goods_name as goods_name,t2.sales_count as sales_count,publisher,category,price,sell_price from (select goods_id,goods_name,sum(sales_volume) as sales_count from tb_goods group by goods_name,goods_id order by sales_count desc) t2 left join tb_goods on t2.goods_id=tb_goods.goods_id";
        List<Map<String, Object>> list = jdbcTemplate.queryForList(sql);
        return list;
    }

    @Override
    public List<Map<String, Object>> getAvgArriveTime() {
        String sql = "select order_id,order_time,receive_time,province from dwd_hot_goods ";
        List<Map<String, Object>> list = jdbcTemplate.queryForList(sql);
        return list;
    }

    @Override
    public List<Map<String, Object>> getCategoryHot() {
        String sql = "select category,sum(sales_volume) as sales_count from tb_goods group by category order by sales_count desc";
        List<Map<String, Object>> list = jdbcTemplate.queryForList(sql);
        return list;
    }

    @Override
    public List<Map<String, Object>> getCommentsData() {
        String sql = "select (sum(f_comments)+sum(n_comments)) as all_comments,sum(f_comments) as good_comments,sum(n_comments) as bad_comments from tb_goods";
        List<Map<String, Object>> list = jdbcTemplate.queryForList(sql);
        return list;
    }

    @Override
    public List<Map<String, Object>> getProvinceInfo() {
        String sql = "select province,category,count(category) as sales_count from dwd_hot_goods group by province,category";
        List<Map<String, Object>> list = jdbcTemplate.queryForList(sql);
        return list;
    }

    @Override
    public List<Map<String, Object>> getUserActionTime() {
        return null;
    }

    @Override
    public List<Map<String, Object>> getUserActionCount() {
        String sql = "select ads_user_info.user_id as user_id,goods_id,tpye_operate,count(tpye_operate) as times from ads_user_info left join tb_alibaba_data on ads_user_info.user_id=tb_alibaba_data.user_id group by ads_user_info.user_id,goods_id,tpye_operate";
        List<Map<String, Object>> list = jdbcTemplate.queryForList(sql);
        return list;
    }

    @Override
    public List<Map<String, Object>> getUserInfo() {
        String sql = "select t1.user_id as user_id,sex,age,register_time,last_login_time,source,province from (select COALESCE(user_id,0,user_id) as user_id,sex,age,register_time,case last_login_time when 0 then register_time else last_login_time end as last_login_time,source from tb_user where user_id!=0) t1 ,dwd_hot_goods t2 where t1.user_id=t2.user_id";
        List<Map<String, Object>> list = jdbcTemplate.queryForList(sql);
        return list;
    }

    @Override
    public List<Map<String, Object>> getUserRefundInfo() {
        String sql = "select ads_user_info.user_id as user_id,reason_frefund from ads_user_info left join tb_refund on ads_user_info.user_id=tb_refund.user_id where reason_frefund is not null group by ads_user_info.user_id,reason_frefund";
        List<Map<String, Object>> list = jdbcTemplate.queryForList(sql);
        return list;
    }

}
