package com.git.recommend.service.imp;

import com.git.recommend.entity.*;
import com.git.recommend.mapper.InsertMysqlMapper;
import com.git.recommend.service.InsertMysqlService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author liangshengyu
 */
@Service
public class InsertMysqlServiceImpl implements InsertMysqlService {

    @Autowired
    private InsertMysqlMapper insertMysqlMapper;


    @Override
    public int insertGoodsTopN(GoodsTopN goodsTopN) {
        insertMysqlMapper.insertGoodsTopN(goodsTopN);
        System.out.println("插入成功");
        return 0;
    }

    @Override
    public int insertAvgArriveTime(AvgArriveTime avgArriveTime) {
        insertMysqlMapper.insertAvgArriveTime(avgArriveTime);
        System.out.println("插入成功");
        return 0;
    }

    @Override
    public int insertCategoryHot(CategoryHot categoryHot) {
        insertMysqlMapper.insertCategoryHot(categoryHot);
        System.out.println("插入成功");
        return 0;
    }

    @Override
    public int insertCommentsData(CommentsData commentsData) {
        insertMysqlMapper.insertCommentsData(commentsData);
        System.out.println("插入成功");
        return 0;
    }

    @Override
    public int insertProvinceInfo(ProvinceInfo provinceInfo) {
        insertMysqlMapper.insertProvinceInfo(provinceInfo);
        System.out.println("插入成功");
        return 0;
    }
    @Override
    public int insertUserActionTime(UserActionTime userActionTime) {
        insertMysqlMapper.insertUserActionTime(userActionTime);
        System.out.println("插入成功");
        return 0;
    }

    @Override
    public int insertUserActionCount(UserActionCount userActionCount) {
        insertMysqlMapper.insertUserActionCount(userActionCount);
        System.out.println("插入成功");
        return 0;
    }

    @Override
    public int insertUserInfo(UserInfo userInfo) {
        insertMysqlMapper.insertUserInfo(userInfo);
        System.out.println("插入成功");
        return 0;
    }
    @Override
    public int insertUserRefundInfo(UserRefundInfo userRefundInfo) {
        insertMysqlMapper.insertUserRefundInfo(userRefundInfo);
        System.out.println("插入成功");
        return 0;
    }
}
