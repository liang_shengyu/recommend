package com.git.recommend.mapper;

import com.git.recommend.entity.*;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface DataMapper {

    /**
     * 获取类型统计数据
     *
     * @return
     */
    List<CategoryData> getCategoryData();

    List<GoodsTopData> getGoodsTopData();

    List<CommentsData> getCommentsData();

    List<ProvinceAvgTime> getProvinceAvgTime();


    /**
     * 通过浏览时间获取推荐产品
     */
    List<RecommendGoods> getRecommendGoodsByWaitTime(@Param(value = "userId") int userId);

    /**
     * 通过用户所在省份获取推荐产品
     */
    List<RecommendGoods> getRecommendGoodsByProvince(@Param(value = "userId") int userId);

    /**
     * 用户画像——用户基本信息
     */
    List<UserInfo> getUserInfo(@Param(value = "userId") int userId);

    /**
     * 用户画像——用户消费结构
     * */
//    List<UserConsumeForm> getUserConsumeForm(@Param(value = "userId") int userID);

    /**
     * 获取所有用户Id
     */
    List<Integer> getAllUserId();

    /**
     * 用户操作类型
     */
    List<UserActionCount> getUserAction(@Param(value = "userId") int userId);

    /**
     * 获取观看类型时间
     */
    List<CategoryTime> getCategoryTime(@Param(value = "userId") int userId);

    /**
     * 获取退款信息
     */
    List<RefundInfo> getRefund(@Param(value = "userId") int userId);

    /**
     * 获取收藏夹
     */
    List<String> getFavorite(@Param(value = "userId") int userId);

    /**
     * 获取购物车
     * @param userId
     * @return
     */
    List<String> getBuyCar(@Param(value = "userId") int userId);
}