package com.git.recommend.mapper;

import com.git.recommend.entity.*;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Mapper
public interface InsertMysqlMapper {
    int insertGoodsTopN(GoodsTopN goodsTopN);
    int insertAvgArriveTime(AvgArriveTime avgArriveTime);
    int insertCategoryHot(CategoryHot categoryHot);
    int insertCommentsData(CommentsData commentsData);
    int insertProvinceInfo(ProvinceInfo provinceInfo);
    int insertUserActionTime(UserActionTime userActionTime);
    int insertUserActionCount(UserActionCount userActionCount);
    int insertUserInfo(UserInfo userInfo);
    int insertUserRefundInfo(UserRefundInfo userRefundInfo);
}
